# acelee-flowable

#### 介绍
springboot2.x集成flowable6.4.0工作流+MySQL5+的简单例子


#### 安装教程
1. 直接用IntelliJ IDEA打开，修改application.properties文件MySQL对应的配置
2. 运行FlowableApplication.java的main()方法启动

#### 使用说明

1. 访问：http://localhost:8081/expense/add?userId=123123&money=123321，创建一个流程
    
    返回：提交成功.流程Id为：7a19811e-3a5a-11e9-8dab-62f677981e8a

2. 访问：http://localhost:8081/expense/list?userId=123123，查询待办列表
    
    控制台输出：Task[id=7a3baf24-3a5a-11e9-8dab-62f677981e8a, name=出差报销]

3. 访问：http://localhost:8081/expense/apply?taskId=7a3baf24-3a5a-11e9-8dab-62f677981e8a，同意
    
    返回：processed ok!

4. 访问：http://localhost:8081/expense/processDiagram?processId=7a19811e-3a5a-11e9-8dab-62f677981e8a，生成流程图
    
    返回：流程图片，在resources目录下的“出差报销.png”
